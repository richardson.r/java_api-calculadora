package br.com.calculadora.calculadora.controllers;

import br.com.calculadora.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.calculadora.models.Calculadora;
import br.com.calculadora.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;

    @GetMapping("")
    public String inicio() {
        return "Calculadora - Exemplo RESTfull";
    }

    @PostMapping("somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() <= 1)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário, pelo menos 2 números para somar.");

        return calculadoraService.somar(calculadora);
    }

    @PostMapping("subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() <= 1)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário, pelo menos 2 números para somar.");

        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("dividir")
    public RespostaDTO dividir(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() <= 1)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário, pelo menos 2 números para somar.");

        if (calculadora.getNumeros().contains(0.0))
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não é possível fazer divisão por 0.");

        return calculadoraService.dividir(calculadora);
    }

    @PostMapping("multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() <= 1)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário, pelo menos 2 números para somar.");

        return calculadoraService.multiplicar(calculadora);
    }
}
