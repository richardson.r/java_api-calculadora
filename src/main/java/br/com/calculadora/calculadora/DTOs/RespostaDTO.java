package br.com.calculadora.calculadora.DTOs;

public class RespostaDTO {
    public Double getResultado() {
        return resultado;
    }

    public void setResultado(Double resultado) {
        this.resultado = resultado;
    }

    private Double resultado;

    public RespostaDTO() {
    }

    public RespostaDTO(Double resultado) {
        this.resultado = resultado;
    }
}
