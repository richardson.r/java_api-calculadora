package br.com.calculadora.calculadora.services;

import br.com.calculadora.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora) {
        Double resultado = 0.0;

        for (Double numero : calculadora.getNumeros()) {
            resultado += numero;
        }

        return new RespostaDTO(resultado);
    }

    public RespostaDTO subtrair(Calculadora calculadora) {
        Double resultado = calculadora.getNumeros().get(0) * 2;

        for (Double numero : calculadora.getNumeros()) {
            resultado -= numero;
        }

        return new RespostaDTO(resultado);
    }

    public RespostaDTO dividir(Calculadora calculadora) {
        Double resultado = calculadora.getNumeros().get(0);
        resultado *= resultado;

        for (Double numero : calculadora.getNumeros()) {
            resultado /= numero;
        }

        return new RespostaDTO(resultado);
    }

    public RespostaDTO multiplicar(Calculadora calculadora) {
        Double resultado = 1.0;

        for (Double numero : calculadora.getNumeros()) {
            resultado *= numero;
        }

        return new RespostaDTO(resultado);
    }
}
