package br.com.calculadora.calculadora.models;

import java.util.List;

public class Calculadora {

    private List<Double> numeros;

    public List<Double> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Double> numeros) {
        this.numeros = numeros;
    }

    public Calculadora() {

    }

}
